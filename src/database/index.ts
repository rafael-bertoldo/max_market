import {createConnection} from 'typeorm'

export default function connection() {
    console.log('Database Connected')
    return createConnection()
}