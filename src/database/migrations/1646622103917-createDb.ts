import {MigrationInterface, QueryRunner} from "typeorm";

export class createDb1646622103917 implements MigrationInterface {
    name = 'createDb1646622103917'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "order_products" ("order_product_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "product_id" character varying NOT NULL, "cart_id" character varying NOT NULL, CONSTRAINT "PK_aee736420686c65eddab475dc26" PRIMARY KEY ("order_product_id"))`);
        await queryRunner.query(`CREATE TABLE "products" ("product_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "product_name" character varying NOT NULL, "product_description" character varying NOT NULL, "product_price" double precision NOT NULL, "cartId" uuid, "orderProductOrderProductId" uuid, CONSTRAINT "UQ_894a8151f2433fca9b81acb2975" UNIQUE ("product_name"), CONSTRAINT "PK_a8940a4bf3b90bd7ac15c8f4dd9" PRIMARY KEY ("product_id"))`);
        await queryRunner.query(`CREATE TABLE "users" ("user_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "user_name" character varying NOT NULL, "user_email" character varying NOT NULL, "user_password" character varying NOT NULL, "user_isAdm" character varying NOT NULL, "user_address" character varying NOT NULL, "user_recovery_token" character varying NOT NULL, CONSTRAINT "UQ_643a0bfb9391001cf11e581bdd6" UNIQUE ("user_email"), CONSTRAINT "PK_96aac72f1574b88752e9fb00089" PRIMARY KEY ("user_id"))`);
        await queryRunner.query(`CREATE TABLE "orders" ("order_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "user_id" character varying NOT NULL, "order_price" double precision NOT NULL, "order_status" character varying NOT NULL DEFAULT 'pending', "userUserId" uuid, CONSTRAINT "PK_cad55b3cb25b38be94d2ce831db" PRIMARY KEY ("order_id"))`);
        await queryRunner.query(`CREATE TABLE "carts" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "cart_status" character varying NOT NULL DEFAULT 'active', "cartUserIdUserId" uuid, "productsIdProductId" uuid, "orderProductOrderProductId" uuid, "orderIdOrderId" uuid, CONSTRAINT "REL_e8cb61ed295607d51ead7bd473" UNIQUE ("orderIdOrderId"), CONSTRAINT "PK_b5f695a59f5ebb50af3c8160816" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "products" ADD CONSTRAINT "FK_7a637e662f7c4b3a85001c53dfd" FOREIGN KEY ("cartId") REFERENCES "carts"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "products" ADD CONSTRAINT "FK_b658d8c5b9812b94f29b41163cb" FOREIGN KEY ("orderProductOrderProductId") REFERENCES "order_products"("order_product_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "orders" ADD CONSTRAINT "FK_6a4ebad71685a4ed11e89b3e834" FOREIGN KEY ("userUserId") REFERENCES "users"("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "carts" ADD CONSTRAINT "FK_7dcd987ff5adc5da58512867ed2" FOREIGN KEY ("cartUserIdUserId") REFERENCES "users"("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "carts" ADD CONSTRAINT "FK_cad135d30a152f8d8e1f58cb4a7" FOREIGN KEY ("productsIdProductId") REFERENCES "products"("product_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "carts" ADD CONSTRAINT "FK_3cd3247785d0df0b782a0d92cc8" FOREIGN KEY ("orderProductOrderProductId") REFERENCES "order_products"("order_product_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "carts" ADD CONSTRAINT "FK_e8cb61ed295607d51ead7bd4734" FOREIGN KEY ("orderIdOrderId") REFERENCES "orders"("order_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "carts" DROP CONSTRAINT "FK_e8cb61ed295607d51ead7bd4734"`);
        await queryRunner.query(`ALTER TABLE "carts" DROP CONSTRAINT "FK_3cd3247785d0df0b782a0d92cc8"`);
        await queryRunner.query(`ALTER TABLE "carts" DROP CONSTRAINT "FK_cad135d30a152f8d8e1f58cb4a7"`);
        await queryRunner.query(`ALTER TABLE "carts" DROP CONSTRAINT "FK_7dcd987ff5adc5da58512867ed2"`);
        await queryRunner.query(`ALTER TABLE "orders" DROP CONSTRAINT "FK_6a4ebad71685a4ed11e89b3e834"`);
        await queryRunner.query(`ALTER TABLE "products" DROP CONSTRAINT "FK_b658d8c5b9812b94f29b41163cb"`);
        await queryRunner.query(`ALTER TABLE "products" DROP CONSTRAINT "FK_7a637e662f7c4b3a85001c53dfd"`);
        await queryRunner.query(`DROP TABLE "carts"`);
        await queryRunner.query(`DROP TABLE "orders"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TABLE "products"`);
        await queryRunner.query(`DROP TABLE "order_products"`);
    }

}
