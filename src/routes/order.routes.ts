import { Router } from "express";
import {
  checkout,
  listAllOrders,
  listOrderById,
} from "../controllers/order.controllers";
import { ensureAuth } from "../middlewares/ensureAuth";

const orderRouter = Router();

orderRouter.use(ensureAuth);
orderRouter.post("/", checkout);
orderRouter.get("/:order_id", listOrderById);
orderRouter.get("/", listAllOrders);

export default orderRouter;
