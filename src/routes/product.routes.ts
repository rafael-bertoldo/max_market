import { Router } from "express";
import {
  createProduct,
  listAll,
  listById,
} from "../controllers/product.controllers";
import { ensureAuth } from "../middlewares/ensureAuth";

const productRouter = Router();

productRouter.use(ensureAuth);
productRouter.get("/", listAll);
productRouter.get("/:product_id", listById);
productRouter.post("/", createProduct);

export default productRouter;
