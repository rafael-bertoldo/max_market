import { Router } from "express";
import { createUser, listAll, listById } from "../controllers/user.controllers";
import { ensureAuth } from "../middlewares/ensureAuth";

const userRouter = Router();

userRouter.post("/", createUser);
userRouter.use(ensureAuth);
userRouter.get("/", listAll);
userRouter.get("/:user_id", listById);

export default userRouter;
