import { Router } from "express";
import { addProduct, listAll, listById } from "../controllers/cart.controllers";
import { ensureAuth } from "../middlewares/ensureAuth";

const cartRouter = Router();


cartRouter.use(ensureAuth);
cartRouter.post("/", addProduct);
cartRouter.get("/", listAll);
cartRouter.get("/:cart_id", listById);

export default cartRouter;
