import { hash } from "bcryptjs";
import { Router } from "express";
import { getCustomRepository } from "typeorm";
import AppError from "../errors/appError";
import { v4 as uuid } from "uuid";
import UserRepository from "../repositories/userRepository";
import { mailOptions, transport } from "../services/email.service";

const mailRouter = Router();

mailRouter.post("/email", async (req, res, next) => {
  const { user_email, email_subject, email_body } = req.body;
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findByEmail(user_email);

  if (!user) {
    throw new AppError("user_email not found", 404);
  }

  const options = mailOptions(user.user_email, email_subject, email_body);

  transport.sendMail(options, function (err, info) {
    if (err) {
      return next(err);
    } else {
      console.log(info);
    }
  });

  return res.json({ message: "email sent with success" });
});
mailRouter.post("/recuperar", async (req, res, next) => {
  const { user_email } = req.body;
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findByEmail(user_email);

  if (!user) {
    throw new AppError("email not found", 404);
  }

  user.user_recovery_token = uuid();
  await userRepository.save(user);

  const options = mailOptions(
    user.user_email,
    "redefinição de senha",
    `token para redefinição de senha ${user.user_recovery_token}`
  );

  transport.sendMail(options, function (err, info) {
    if (err) {
      return next(err);
    } else {
      console.log(info);
    }
  });

  return res.send({ message: "recovery token sent with success" });
});

mailRouter.post("/alterar_senha", async (req, res) => {
  const { user_email, new_password, user_recovery_token } = req.body;
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findByEmail(user_email);

  if (!user) {
    throw new AppError("email not found", 404);
  }

  if (user.user_recovery_token !== user_recovery_token) {
    throw new AppError("invalid user_recovery_token", 401);
  }

  user.user_password = await hash(new_password, 8);
  user.user_recovery_token = "";

  userRepository.save(user);

  return res.send({ message: "password updated with success" });
});

export default mailRouter;
