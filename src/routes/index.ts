import { Router } from "express";
import cartRouter from "./cart.routes";
import mailRouter from "./mail.routes";
import orderRouter from "./order.routes";
import productRouter from "./product.routes";
import sessionRouter from "./session.routes";
import userRouter from "./user.routes";

const routes = Router();

routes.use("/", mailRouter);
routes.use("/user", userRouter);
routes.use("/product", productRouter);
routes.use("/login", sessionRouter);
routes.use("/cart", cartRouter);
routes.use("/buy", orderRouter);

export default routes;
