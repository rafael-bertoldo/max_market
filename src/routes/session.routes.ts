import { Router } from "express";
import { login } from "../controllers/session.controllers";

const sessionRouter = Router();

sessionRouter.post("/", login);

export default sessionRouter;
