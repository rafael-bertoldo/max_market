import { EntityRepository, Repository } from "typeorm";
import User from "../models/user";

@EntityRepository(User)
export default class UserRepository extends Repository<User> {
  public async findByEmail(user_email: string) {
    const user = this.findOne({
      where: {
        user_email,
      },
    });
    return user;
  }
}
