import { Exclude } from "class-transformer";
import { IsBoolean, IsNotEmpty, IsString, MinLength } from "class-validator";
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import Cart from "./cart";
import Order from "./order";

@Entity("users")
export default class User {
  @PrimaryGeneratedColumn("uuid")
  user_id: string;

  @Column()
  @IsString({ message: "user_name must be a string" })
  user_name: string;

  @Column({ unique: true, nullable: false })
  @IsString({ message: "user_email must be a string" })
  user_email: string;

  @Exclude()
  @Column()
  @IsString({ message: "user_password must be a string" })
  user_password: string;

  @Column({ nullable: false })
  @IsBoolean({ message: "user_isAdm must be a boolean value" })
  user_isAdm: string;

  @Column()
  @IsString({ message: "user_addres must be a string" })
  user_address: string;

  @Exclude()
  @Column()
  @IsString({ message: "user_recorey_token must be a string" })
  user_recovery_token: string;

  @OneToMany(() => Cart, (cart) => cart.cart_user_id)
  @JoinColumn()
  cart: Cart;

  @OneToMany(() => Order, (order) => order.user_id)
  order: Order;
}
