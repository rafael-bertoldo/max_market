import { IsNotEmpty, IsString } from "class-validator";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import Cart from "./cart";
import OrderProduct from "./order_product";

@Entity("products")
export default class Product {
  @PrimaryGeneratedColumn("uuid")
  product_id: string;

  @ManyToOne(() => Cart, (cart) => cart.products_id)
  cart: Cart;

  @ManyToOne(() => OrderProduct, (orderProduct) => orderProduct.productId)
  order_product: OrderProduct;

  @Column({ nullable: false, unique: true })
  @IsString({ message: "product_name must be a string" })
  @IsNotEmpty({ message: "product_name cannot must be empty" })
  product_name: string;

  @Column()
  @IsString({ message: "product_description must be a string" })
  product_description: string;

  @Column({ type: "float" })
  @IsNotEmpty({ message: "product_price cannot be empty" })
  product_price: number;
}
