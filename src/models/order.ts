import { IsString } from "class-validator";
import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import Cart from "./cart";
import Product from "./product";
import User from "./user";

@Entity("orders")
export default class Order {
  @PrimaryGeneratedColumn("uuid")
  order_id: string;

  @OneToMany(() => Product, (product) => product.product_id)
  product_ids: Product[];

  @ManyToOne(() => User, (user) => user.user_id)
  user: User;

  @Column()
  user_id: string;

  @Column({ type: "float" })
  order_price: number;

  @Column({ default: "pending" })
  @IsString({ message: "order_status must be a string" })
  order_status: string;
}
