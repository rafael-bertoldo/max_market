import { IsString } from "class-validator";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import Order from "./order";
import OrderProduct from "./order_product";
import Product from "./product";
import User from "./user";

@Entity("carts")
export default class Cart {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(() => User, (user) => user.user_id, { eager: true })
  cart_user_id?: User;

  @ManyToOne(() => Product)
  products_id?: Product[];

  @ManyToOne(() => OrderProduct, (orderProduct) => orderProduct.cartId)
  order_product: OrderProduct;

  @Column({ default: "active" })
  @IsString({ message: "cart_status must be a string" })
  cart_status: string;

  @OneToOne(() => Order)
  @JoinColumn()
  order_id: Order;
}
