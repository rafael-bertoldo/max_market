import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import Cart from "./cart";
import Product from "./product";

@Entity("order_products")
export default class OrderProduct {
  @PrimaryGeneratedColumn("uuid")
  order_product_id: string;

  @OneToMany(() => Product, (product) => product.product_id)
  @JoinColumn()
  productId: Product;

  @OneToMany(() => Cart, (cart) => cart.id)
  cartId: Cart;

  @Column()
  product_id: string;

  @Column()
  cart_id: string;
}
