import { classToClass } from "class-transformer";
import { NextFunction, Request, Response } from "express";
import { authService } from "../services/session.services";

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const authUser = await authService(req.body);

    return res.json(classToClass(authUser));
  } catch (err) {
    next(err);
  }
};
