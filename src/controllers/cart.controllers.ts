import { NextFunction, Request, Response } from "express";
import { getCustomRepository, getRepository } from "typeorm";
import AppError from "../errors/appError";
import Cart from "../models/cart";
import Order from "../models/order";
import OrderProduct from "../models/order_product";
import Product from "../models/product";
import UserRepository from "../repositories/userRepository";

export const addProduct = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { product_id } = req.body;
    const user_id = req.user;
    const userRepository = getCustomRepository(UserRepository);
    const productRepository = getRepository(Product);
    const orderProductRepository = getRepository(OrderProduct);
    const cartRepository = getRepository(Cart);
    const user = await userRepository.findOne(user_id.id);

    if (!user) {
      throw new AppError("user not found", 404);
    }

    const checkProduct = await productRepository.findOne(product_id);

    if (!checkProduct) {
      throw new AppError("product not found", 404);
    }
    const cart = await cartRepository.findOne({
      where: {
        cart_user_id: user_id.id,
      },
    });

    const op = {
      cart_id: cart?.id,
      product_id,
    };

    const orderProduct = orderProductRepository.create(op);

    await orderProductRepository.save(orderProduct);

    return res.json({ message: "product succssefuly added to cart" });
  } catch (err) {
    next(err);
  }
};

export const listAll = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user = await getCustomRepository(UserRepository).findOne({
    where: { user_id: req.user.id },
  });

  if (user?.user_isAdm === "false") {
    return res
      .status(401)
      .send({ message: "only adm users can list all carts" });
  }

  try {
    const cartRepository = getRepository(Cart);
    const carts = await cartRepository.find();

    return res.send(carts);
  } catch (e) {
    next(e);
  }
};

export const listById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { cart_id } = req.params;
    const user = await getCustomRepository(UserRepository).findOne({
      where: { user_id: req.user.id },
    });
    const cartRepository = getRepository(Cart);

    if (user?.user_isAdm === "false") {
      const cartSelf = await cartRepository.findOne({
        where: {
          cart_user_id: req.user.id,
        },
      });

      return res.send(cartSelf);
    }

    const cart = await cartRepository.findOne({
      where: {
        id: cart_id,
      },
    });

    if (!cart) {
      throw new AppError("cart not found", 404);
    }

    return res.send(cart);
  } catch (e) {
    next(e);
  }
};
