import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import AppError from "../errors/appError";
import Cart from "../models/cart";
import Order from "../models/order";
import OrderProduct from "../models/order_product";
import Product from "../models/product";
import User from "../models/user";

export const checkout = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const OrderRepository = getRepository(Order);
    const orderProductRepository = getRepository(OrderProduct);
    const userRepository = getRepository(User);
    const cartRepository = getRepository(Cart);
    const productRepository = getRepository(Product);

    const user = await userRepository.findOne({
      where: {
        user_id: req.user.id,
      },
    });

    const cart = await cartRepository.findOne({
      where: {
        cart_user_id: user?.user_id,
      },
    });

    const products = await orderProductRepository.find({
      where: {
        cart_id: cart?.id,
      },
    });

    const order = OrderRepository.create({
      order_price: 0,
      user_id: user?.user_id,
    });

    products.forEach(async (item) => {
      const foundedItem = await productRepository.findOne({
        where: {
          product_id: item.product_id,
        },
      });

      if (!foundedItem) {
        throw new AppError("product not found", 404);
      }

      order.order_price += foundedItem.product_price;
      await OrderRepository.save(order);
    });

    return res.json({ message: "generated order with success" });
  } catch (err) {
    next(err);
  }
};

export const listOrderById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const order_id = req.params;
    const user_id = req.user.id;
    const userRepository = getRepository(User);
    const orderRepository = getRepository(Order);

    const user = await userRepository.findOne(user_id);

    if (user?.user_isAdm === "false") {
      const selfOrder = orderRepository.findOne({
        where: {
          user_id: user.user_id,
        },
      });

      return res.send(selfOrder);
    }

    const order = await orderRepository.findOne(order_id);

    return res.send(order);
  } catch (err) {
    next(err);
  }
};

export const listAllOrders = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const orderRepository = getRepository(Order);
    const userRepository = getRepository(User);
    const user = await userRepository.findOne({
      where: {
        user_id: req.user.id,
      },
    });

    if (user?.user_isAdm === "false") {
      throw new AppError("only adm users can list all orders");
    }

    const orders = await orderRepository.find();

    return res.send(orders);
  } catch (err) {
    next(err);
  }
};
