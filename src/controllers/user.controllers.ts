import { classToClass } from "class-transformer";
import { NextFunction, Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import AppError from "../errors/appError";
import UserRepository from "../repositories/userRepository";
import { createCarts } from "../services/cart.service";
import { createUsers } from "../services/user.service";

export const createUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const data = req.body;
    const user = await createUsers(data);
    const cart_user_id = user.user_id;
    const cart = await createCarts({ cart_user_id });

    return res.status(201).send(classToClass(user));
  } catch (err) {
    next(err);
  }
};

export const listAll = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.findOne({
      where: {
        user_id: req.user.id,
      },
    });

    if (user?.user_isAdm === "false") {
      return res
        .status(401)
        .send({ message: "only adm users can list all users" });
    }
    const users = await userRepository.find();

    return res.send(classToClass(users));
  } catch (err) {
    next(err);
  }
};

export const listById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.findOne({
      where: {
        user_id: req.user.id,
      },
    });

    if (user?.user_isAdm === "false") {
      return res.send(classToClass(user));
    }

    const { user_id } = req.params;

    const userWithParams = await userRepository.findOne({
      where: {
        user_id,
      },
    });

    if (!userWithParams) {
      throw new AppError("User not found", 404);
    }

    return res.send(classToClass(userWithParams));
  } catch (err) {
    next(err);
  }
};
