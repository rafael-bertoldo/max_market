import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import Product from "../models/product";
import User from "../models/user";
import { createProducts } from "../services/product.service";

export const createProduct = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await getRepository(User).findOne({
      where: { user_id: req.user.id },
    });

    if (user?.user_isAdm === "false") {
      return res
        .status(401)
        .send({ message: "only adm users can create a product" });
    }
    const data = req.body;
    const product = await createProducts(data);

    return res.status(201).send(product);
  } catch (err) {
    next(err);
  }
};

export const listAll = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const productRepository = getRepository(Product);
  const products = await productRepository.find();

  return res.send(products);
};

export const listById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { product_id } = req.params;
  const productRepository = getRepository(Product);
  const product = await productRepository.findOne(product_id);

  return res.send(product);
};
