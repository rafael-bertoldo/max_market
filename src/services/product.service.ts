import { getRepository } from "typeorm";
import AppError from "../errors/appError";
import Product from "../models/product";

interface CreateRequest {
  product_name: string;
  product_description: string;
  product_price: number;
}

interface CreateResponse {
  product_id: string;
  product_name: string;
  product_description: string;
  product_price: string;
}

export const createProducts = async ({
  product_name,
  product_description,
  product_price,
}: CreateRequest) => {
  const productRepository = getRepository(Product);
  const checkProduct = await productRepository.findOne({
    where: {
      product_name,
    },
  });

  if (checkProduct) {
    throw new AppError("Product already exists");
  }

  const product = productRepository.create({
    product_name,
    product_description,
    product_price,
  });

  await productRepository.save(product);

  return product;
};
