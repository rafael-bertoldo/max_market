import { compare } from "bcryptjs";
import { getCustomRepository } from "typeorm";
import AppError from "../errors/appError";
import User from "../models/user";
import UserRespository from "../repositories/userRepository";
import authConfig from "../config/auth";
import { sign } from "jsonwebtoken";

interface Request {
  email: string;
  password: string;
}

interface Response {
  token: string;
  user: User;
}

export const authService = async ({
  email,
  password,
}: Request): Promise<Response> => {
  const userRepository = getCustomRepository(UserRespository);
  const user = await userRepository.findByEmail(email);

  if (!user) {
    throw new AppError("Wrong email/password", 401);
  }

  const passwordMatch = await compare(password, user.user_password);

  if (!passwordMatch) {
    throw new AppError("Wrong email/password", 401);
  }

  const { secret, expiresIn } = authConfig.jwt;

  const token = sign({}, secret, {
    subject: user.user_id,
    expiresIn,
  });

  return {
    user,
    token,
  };
};
