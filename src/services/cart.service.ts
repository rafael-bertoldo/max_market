import { getCustomRepository, getRepository } from "typeorm";
import Cart from "../models/cart";
import User from "../models/user";
import UserRepository from "../repositories/userRepository";

interface CreateRequest {
  cart_user_id: string;
}

interface CreateResponse {
  cart_status: string;
}

export const createCarts = async ({ cart_user_id }: CreateRequest) => {
  const cartRepository = getRepository(Cart);
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findOne({
    where: {
      user_id: cart_user_id,
    },
  });

  const cart = cartRepository.create({ cart_user_id: user });
  await cartRepository.save(cart);

  return cart;
};
