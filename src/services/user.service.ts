import { hash } from "bcryptjs";
import { getCustomRepository } from "typeorm";
import AppError from "../errors/appError";
import UserRepository from "../repositories/userRepository";
import { v4 as uuid } from "uuid";

interface CreateRequest {
  user_name: string;
  user_email: string;
  user_password: string;
  user_isAdm: string;
  user_address: string;
}

interface CreateResponse {
  user_id: string;
  user_name: string;
  user_email: string;
  user_isAdm: string;
  user_address: string;
}

export const createUsers = async ({
  user_name,
  user_email,
  user_password,
  user_address,
  user_isAdm,
}: CreateRequest): Promise<CreateResponse> => {
  const userRepository = getCustomRepository(UserRepository);

  const checkUser = await userRepository.findByEmail(user_email);

  if (checkUser) {
    throw new AppError("Email already exists");
  }

  const hashedPassword = await hash(user_password, 8);
  const user = userRepository.create({
    user_name,
    user_email,
    user_password: hashedPassword,
    user_isAdm,
    user_address,
    user_recovery_token: uuid(),
  });

  await userRepository.save(user);

  return user;
};
