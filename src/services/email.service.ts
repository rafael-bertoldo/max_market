import nodemailer from "nodemailer";

export const transport = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "2d043a50e5702d",
    pass: "150ad45a3da4d5",
  },
});

export const mailOptions = (to: string, subject: string, text: string) => {
  return {
    from: "no-reply@max-market.com.br",
    to,
    subject,
    text,
  };
};
